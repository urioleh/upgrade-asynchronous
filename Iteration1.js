// 1.1 Utiliza esta url de la api Agify 'https://api.agify.io?name=michael' para 
// hacer un .fetch() y recibir los datos que devuelve. Imprimelo mediante un 
// console.log(). Para ello, es necesario que crees un .html y un .js.

const url ='https://api.agify.io?name=michael'

//.THEN

const imprimeUrl1 = (url) =>{
fetch(url)
    .then((response) => response.json())
    .then((myJson) => console.log(myJson))
}

// ASYNC / AWAIT

imprimeUrl1(url)

const imprimeUrl2 = async (url) => {
    let response = await fetch(url);
    let myJson = await response.json();
    console.log(myJson)
}
    
imprimeUrl2(url)
    

