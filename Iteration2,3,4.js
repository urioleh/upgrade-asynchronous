// 2.1 Dado el siguiente javascript y html. Añade la funcionalidad necesaria usando 
// fetch() para hacer una consulta a la api cuando se haga click en el botón, 
// pasando como parametro de la api, el valor del input.

// 2.3 En base al ejercicio anterior. Crea dinamicamente un elemento  por cada petición 
// a la api que diga...'El nombre X tiene un Y porciento de ser de Z' etc etc.
// EJ: El nombre Pepe tiene un 22 porciento de ser de ET y un 6 porciento de ser 
// de MZ.

// 2.4 En base al ejercicio anterior, crea un botón con el texto 'X' para cada uno 
// de los p que hayas insertado y que si el usuario hace click en este botón 
// eliminemos el parrafo asociado.

const baseUrl = 'https://api.nationalize.io';




const btnListener = ()=>{
    const input = document.getElementById('input')
    let name = input.value;
    
   fetch(`${baseUrl}?name=${name}`)
   .then((response) => response.json())
   .then((myJson)=> {
       console.log(myJson)
      const body = document.querySelector('body');
      const div = document.createElement('div')
      body.appendChild(div)
      div.innerHTML = ""
      const country = myJson.country
      console.log(country)
      country.forEach(element => {
          const p = document.createElement('p');
          const button = document.createElement('button')
          button.innerText ='X';
          p.innerHTML = `El nombre <strong>${myJson.name}</strong> tiene un <strong>${element.probability}</strong> 
          porciento de probabilidades de ser de <strong>${element.country_id}</strong>`;
          div.appendChild(p)
          div.appendChild(p)
          div.appendChild(button) 
          button.addEventListener('click', () =>{
            p.remove()
            button.remove()
          })
          
      });
     
   })
}

const getButton = () =>{
    const button = document.getElementById('btn')
    button.addEventListener('click', btnListener)

}

window.onload = () => {
    getButton(baseUrl)
    
}
